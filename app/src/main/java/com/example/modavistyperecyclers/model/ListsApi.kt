package com.example.modavistyperecyclers.model

interface ListsApi {
    suspend fun getTypeList(): List<String>
    suspend fun getInts(): List<String>
    suspend fun getDoubles(): List<String>
    suspend fun getChars(): List<String>
}
package com.example.modavistyperecyclers.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object ListsRepo {
    private val typeList: List<String> = listOf("Ints", "Doubles", "Chars")
    private val charList: List<String> = listOf("A", "B", "C", "D", "E", "F", "G", "H", "I", "j")
    private val intList: List<String> = listOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")
    private val doubleList: List<String> =
        listOf("1.2", "2.3", "3.4", "4.5", "5.6", "6.7", "7.8", "8.9", "9.1", "9.2")
    private val typeListApi = object : ListsApi {
        override suspend fun getTypeList(): List<String> {
            return typeList
        }

        override suspend fun getChars(): List<String> {
            return charList
        }

        override suspend fun getInts(): List<String> {
            return intList
        }

        override suspend fun getDoubles(): List<String> {
            return doubleList
        }
    }

    suspend fun getTypeList(): List<String> = withContext(Dispatchers.IO) {
        delay(500)
        typeListApi.getTypeList()
    }

    suspend fun getChars(): List<String> = withContext(Dispatchers.IO) {
        delay(500)
        typeListApi.getChars()
    }

    suspend fun getInts(): List<String> = withContext(Dispatchers.IO) {
        delay(500)
        typeListApi.getInts()
    }

    suspend fun getDoubles(): List<String> = withContext(Dispatchers.IO) {
        delay(500)
        typeListApi.getDoubles()
    }
}
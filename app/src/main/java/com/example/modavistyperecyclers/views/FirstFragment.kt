package com.example.modavistyperecyclers.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.modavistyperecyclers.databinding.FragmentFirstBinding
import com.example.modavistyperecyclers.viewmodel.ListsViewModel

class FirstFragment : Fragment() {
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    private val listsViewModel by viewModels<ListsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFirstBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listsViewModel.getTypeList()
        listsViewModel.typeList.observe(viewLifecycleOwner) { type ->
            binding.typesStaggered.apply {
                adapter = TypeAdapter().apply {
                    addType(type)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}
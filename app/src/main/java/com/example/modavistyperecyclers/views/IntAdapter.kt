package com.example.modavistyperecyclers.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.modavistyperecyclers.databinding.ItemSelectedTypeListBinding

class IntAdapter : RecyclerView.Adapter<IntAdapter.IntViewHolder>() {

    private var ints = mutableListOf<String>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): IntViewHolder {
        val binding = ItemSelectedTypeListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return IntViewHolder(binding)
    }

    override fun onBindViewHolder(intViewHolder: IntViewHolder, position: Int) {
        val int = ints[position]
        intViewHolder.loadInt(int)
    }

    override fun getItemCount(): Int {
        return ints.size
    }

    fun addInt(int: List<String>){
        this.ints = int.toMutableList()
        notifyDataSetChanged()
    }

    class IntViewHolder(
        private val binding: ItemSelectedTypeListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun loadInt(int: String) {
            binding.itemSelectedTypeText.text = int
        }
    }
}
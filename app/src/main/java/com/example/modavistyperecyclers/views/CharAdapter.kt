package com.example.modavistyperecyclers.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.modavistyperecyclers.databinding.ItemSelectedTypeListBinding

class CharAdapter : RecyclerView.Adapter<CharAdapter.CharViewHolder>() {
    private var chars = mutableListOf<String>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CharViewHolder {
        val binding = ItemSelectedTypeListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return CharViewHolder(binding)
    }

    override fun onBindViewHolder(charViewHolder: CharViewHolder, position: Int) {
        val char = chars[position]
        charViewHolder.loadChar(char)
    }

    override fun getItemCount(): Int {
        return chars.size
    }

    fun addChars(chars: List<String>) {
        this.chars = chars.toMutableList()
        notifyDataSetChanged()
    }

    class CharViewHolder(
        private val binding: ItemSelectedTypeListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun loadChar(char: String) {
            binding.itemSelectedTypeText.text = char
        }
    }
}
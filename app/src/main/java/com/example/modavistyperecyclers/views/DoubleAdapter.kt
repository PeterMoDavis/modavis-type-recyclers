package com.example.modavistyperecyclers.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.modavistyperecyclers.databinding.ItemSelectedTypeListBinding

class DoubleAdapter : RecyclerView.Adapter<DoubleAdapter.DoubleViewHolder>() {
    private var doubles = mutableListOf<String>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DoubleViewHolder {
        val binding = ItemSelectedTypeListBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DoubleViewHolder(binding)
    }

    override fun onBindViewHolder(doubleViewHolder: DoubleViewHolder, position: Int) {
        val double = doubles[position]
        doubleViewHolder.loadDoubles(double)
    }

    override fun getItemCount(): Int {
        return doubles.size
    }

    fun addDoubles(doubles: List<String>) {
        this.doubles = doubles.toMutableList()
        notifyDataSetChanged()
    }

    class DoubleViewHolder(
        private val binding: ItemSelectedTypeListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun loadDoubles(double: String) {
            binding.itemSelectedTypeText.text = double
        }
    }
}
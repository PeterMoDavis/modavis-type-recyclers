package com.example.modavistyperecyclers.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.modavistyperecyclers.databinding.FragmentIntBinding
import com.example.modavistyperecyclers.viewmodel.ListsViewModel

class IntFragment : Fragment() {
    private var _binding: FragmentIntBinding? = null
    private val binding get() = _binding!!
    private val listsViewModel by viewModels<ListsViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentIntBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnIntBack.setOnClickListener {
            findNavController().navigateUp()
        }
        with(binding.intList) {
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
            adapter = IntAdapter().apply {
                with(listsViewModel) {
                    getIntList()
                    intList.observe(viewLifecycleOwner) {
                        addInt(it)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
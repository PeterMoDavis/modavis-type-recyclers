package com.example.modavistyperecyclers.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.modavistyperecyclers.databinding.ItemTypesBinding

class TypeAdapter : RecyclerView.Adapter<TypeAdapter.TypeViewHolder>() {

    private var types = mutableListOf<String>()


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TypeViewHolder {
        val binding = ItemTypesBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return TypeViewHolder(binding)
    }

    override fun onBindViewHolder(typeViewHolder: TypeViewHolder, position: Int) {
        val type = types[position]
        typeViewHolder.loadType(type)
    }

    override fun getItemCount(): Int {
        return types.size
    }

    fun addType(type: List<String>) {
        this.types = type.toMutableList()
        notifyDataSetChanged()
    }


    class TypeViewHolder(
        private val binding: ItemTypesBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun loadType(type: String) {
            binding.itemTypeText.text = type

            if (type == "Chars")
                binding.itemTypeText.setOnClickListener {
                    it.findNavController()
                        .navigate(FirstFragmentDirections.actionFirstFragmentToCharFragment())
                }
            else if (type == "Ints")
                binding.itemTypeText.setOnClickListener {
                    it.findNavController()
                        .navigate(FirstFragmentDirections.actionFirstFragmentToIntFragment())
                }
            else {
                binding.itemTypeText.setOnClickListener {
                    it.findNavController()
                        .navigate(FirstFragmentDirections.actionFirstFragmentToDoubleFragment())
                }
            }
        }

    }

}
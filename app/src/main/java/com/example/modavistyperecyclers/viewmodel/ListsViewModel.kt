package com.example.modavistyperecyclers.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.modavistyperecyclers.model.ListsRepo
import kotlinx.coroutines.launch

class ListsViewModel : ViewModel() {
    private val repo = ListsRepo

    private val _typeList = MutableLiveData<List<String>>()
    val typeList: LiveData<List<String>> get() = _typeList

    private val _charList = MutableLiveData<List<String>>()
    val charList: LiveData<List<String>> = _charList

    private val _intList = MutableLiveData<List<String>>()
    val intList: LiveData<List<String>> = _intList

    private val _doubleList = MutableLiveData<List<String>>()
    val doubleList: LiveData<List<String>> = _doubleList

    fun getTypeList() {
        viewModelScope.launch {
            val typeList = repo.getTypeList()
            _typeList.value = typeList
        }
    }

    fun getCharList() {
        viewModelScope.launch {
            val charList = repo.getChars()
            _charList.value = charList
        }

    }

    fun getIntList() {
        viewModelScope.launch {
            val intList = repo.getInts()
            _intList.value = intList
        }
    }

    fun getDoubleList() {
        viewModelScope.launch {
            val doubleList = repo.getDoubles()
            _doubleList.value = doubleList
        }
    }

}